From 33868442087aac6f26f18aeafd527c1a75946f34 Mon Sep 17 00:00:00 2001
From: Nicolas Dufresne <nicolas.dufresne@collabora.com>
Date: Wed, 17 Jan 2024 12:50:34 +0100
Subject: [PATCH 5/5] h265parser: Fix possible overflow using
 max_sub_layers_minus1

This fixes a possible overflow that can be triggered by an invalid value of
max_sub_layers_minus1 being set in the bitstream. The bitstream uses 3 bits,
but the allowed range is 0 to 6 only.

Fixes ZDI-CAN-21768, CVE-2023-40476

Fixes https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/2895

Part-of: <https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/5364>
---
 gst-libs/gst/codecparsers/gsth265parser.c | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/gst-libs/gst/codecparsers/gsth265parser.c b/gst-libs/gst/codecparsers/gsth265parser.c
index 16fce006b..2e8ef182b 100644
--- a/gst-libs/gst/codecparsers/gsth265parser.c
+++ b/gst-libs/gst/codecparsers/gsth265parser.c
@@ -1490,6 +1490,7 @@ gst_h265_parse_vps (GstH265NalUnit * nalu, GstH265VPS * vps)
 
   READ_UINT8 (&nr, vps->max_layers_minus1, 6);
   READ_UINT8 (&nr, vps->max_sub_layers_minus1, 3);
+  CHECK_ALLOWED (vps->max_sub_layers_minus1, 0, 6);
   READ_UINT8 (&nr, vps->temporal_id_nesting_flag, 1);
 
   /* skip reserved_0xffff_16bits */
@@ -1669,6 +1670,7 @@ gst_h265_parse_sps (GstH265Parser * parser, GstH265NalUnit * nalu,
   sps->vps = vps;
 
   READ_UINT8 (&nr, sps->max_sub_layers_minus1, 3);
+  CHECK_ALLOWED (sps->max_sub_layers_minus1, 0, 6);
   READ_UINT8 (&nr, sps->temporal_id_nesting_flag, 1);
 
   if (!gst_h265_parse_profile_tier_level (&sps->profile_tier_level, &nr,
-- 
2.43.0

